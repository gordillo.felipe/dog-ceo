import React, { useState, useEffect } from "react";
import Selectaux from "./Components/Select";
import Cards from "./Components/Card";
import { Container } from "@material-ui/core"
import getDog from "./Services/getDogs";

const razainicial = [{
  image: "",
  breedName: ""

}]

function App() {
  const [raza, setRaza] = useState(razainicial)
  useEffect(() => {
    updateDog();
  }, [])
  const updateDog = (breedName) => {
    getDog(breedName)
      .then((dogimage) => {
        setRaza(dogimage)
      })
  }

  return (
    <div className="App">
      <Container fixed >
        <div className="fixed text-center">
         <p> Seleccione la Raza de su Preferencia:</p> <Selectaux updateDog={updateDog} />
          <hr></hr>
        </div>
        <div>
        <Cards raza={raza} />
        </div>
      </Container>
    </div>
  );
}

export default App;
