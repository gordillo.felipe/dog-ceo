import React from 'react'
import { Card,CardContent,Typography,  CardMedia } from '@material-ui/core';


import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles({
    gameRoot: {
        maxWidth: 345,
    },
    media: {
        height: 0,
        paddingTop: '100%', // 16:9
    }
});
const Cards = ({ raza }) => {

    const classes = useStyles();
    return raza.map((data, index) => {
        return (

            <div className='flexGrow' key={index}>
                <Card className={classes.gameRoot}>

                    
                        <CardMedia
                            component="img"
                            classes={{ root: classes.gameCover }}
                            image={data.image}
                            title={data.valuie}
                        />
                   
                    <CardContent>
                        <Typography variant="body2" color="textSecondary" component="span">
                            {data.breedName}
                        </Typography>
                    </CardContent>
                </Card>
            </div>


        )
    })
}

export default Cards