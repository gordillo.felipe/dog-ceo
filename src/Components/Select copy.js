
import React, { useState, useEffect } from 'react';


import getBreeds from '../Services/getBreeds';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

const initialBreeds = [
    
];

const Selectaux = ({updateDog}) => {

    const [breeds, setBreeds] = useState(initialBreeds);

    useEffect(() => {
        updateBreeds();
    }, []);

    const updateBreeds = () => {
        getBreeds()
            .then((newbreeds) => {
                setBreeds(newbreeds)
            })
    }
  
    return (
        <div className="container">
            <Select  labelId="0" id="select" onChange={(e)=> updateDog(e.target.value)} value="affenpinscher">
            <MenuItem value="" selected>Selecccione una Raza de Perro</MenuItem>
                {breeds.map((breed) => (
                    <MenuItem value={breed} key={breed}>{breed}</MenuItem>
                ))}
            </Select>
                

        </div>
    )
}

export default Selectaux
