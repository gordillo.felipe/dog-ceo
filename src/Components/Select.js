
import React, { useState, useEffect } from 'react';
import getBreeds from '../Services/getBreeds';
import Select from "react-select";
const initialBreeds = [

];


const Selectaux = ({ updateDog }) => {
    const [breeds, setBreeds] = useState(initialBreeds);
    const [selectedOption, setselectedOption] = useState();

    useEffect(() => {
        updateBreeds();


    }, []);
    const updateBreeds = () => {
        getBreeds()
            .then((newbreeds) => {
                setBreeds(newbreeds)

            })
    }
    const handleChange = selectedOption => {
        setselectedOption(selectedOption)

        updateDog(selectedOption)
    };
    return (

        <div className="container">
            <Select
                isMulti
                options={breeds}
                value={selectedOption}
                onChange={handleChange}
                closeMenuOnSelect={false}
            />


        </div>

    )
}

export default Selectaux
