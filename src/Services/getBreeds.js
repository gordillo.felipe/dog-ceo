const getBreeds = async () => {

  const url = "https://dog.ceo/api/breeds/list/all";
  const response = await fetch(url);
  const breedslist = await response.json();
  const data_raw = breedslist.message

  let data_all = []

  for (let i in data_raw) {
    if (data_raw[i].length !== 0) {
      if (`${data_raw[i]}`.includes(",")) {
        const split_breeds = `${data_raw[i]}`.split(',')
        split_breeds.forEach((item, index) => {
          data_all.push({ value: `${i}/${item}`, label: `${i}/${item}` })
        });
      } else {
        data_all.push({ value: `${i}/${data_raw[i]}`, label: `${i}/${data_raw[i]}` })
      }
    } else {
      // Otherwise, the name will be just the breed
      data_all.push({ value: `${i}`, label: `${i}` })
    }

  }
  return data_all
}
const test = {"affenpinscher": [],
        "african": [],
        "airedale": [],
        "akita": [],
        "appenzeller": [],
        "australian": [
            "shepherd"
        ],
        "basenji": []};

//mejora al manejo de los breeds
let test4 =[]
const test2 = Object.keys(test).map((a,index) => {
  
  //console.log(a,index)
  if(test[a].length !==0){
   const test3 = test[a].map(b =>{
     test4.push({value: a+"/"+b})
   })
  }else{
    test4.push({value:a})
  }

})
console.log(test4)
export default getBreeds;
