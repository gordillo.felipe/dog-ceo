const getDog = async (breedName) => {

  let Dog = []
  let url;
  let breednameaux

  if (!breedName || breedName.length === 0) {
    url = "https://dog.ceo/api/breeds/image/random"
    let response = await fetch(url);
    let dogimages = await response.json();
    let data_raw = dogimages.message
    breednameaux = data_raw.split("/")

    Dog.push({
      image: data_raw,
      breedName: breednameaux[4]
    })
    return Dog;
  }
  else {

    for (let i = 0; i < breedName.length; i++) {
      const response = await fetch(`https://dog.ceo/api/breed/${breedName[i].value}/images/random`)
      const dogimages = await response.json();
      let data_raw = dogimages.message
      breednameaux = data_raw.split("/")
      Dog.push({
        image: data_raw,
        breedName: breednameaux[4]
      })

    }
    return Dog;
  }
}

export default getDog;